const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

const isProduction = process.env.NODE_ENV === 'production';
const APP_DIR = path.resolve(__dirname, 'src');
const STATIC_DIR = path.resolve(__dirname, 'static', 'app' );
const BUILD_DIR = path.resolve(__dirname, 'dist', 'app');

const extractSass = new ExtractTextPlugin({
  filename: 'style.css',
});

module.exports = {
  entry: {
    main: `${APP_DIR}/app.tsx`,
    vendor: [
      'react',
      'react-dom',
      'react-redux',
      'react-router',
      'redux'
    ]
  },
  output: {
    path: BUILD_DIR,
    filename: '[name].[chunkhash].js',
    publicPath: '/',
  },
  target: 'web',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'awesome-typescript-loader?module=es6',
            options: {
              configFileName: 'tsconfig.json',
            },
          },
        ],
        include: path.join(__dirname, 'src'),
      },
      {
        test: /(\.scss|\.css)$/,
        use: extractSass.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                url: false
              }
            },
            {
              loader: 'sass-loader',
              options: {
                url: false
              }
            }
          ],
          // use style-loader in development
          fallback: 'style-loader'
        })
      },
      { test: /\.html$/, use: 'html-loader' },
      { test: /\.png$/, use: 'url-loader?limit=10000' },
      { test: /\.jpg$|\.woff2?$|\.ttf$|\.eot$|\.svg$/, use: 'file-loader' },
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': isProduction === true ?
        JSON.stringify('production') : JSON.stringify('development')
    }),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'vendor.bundle.js',
      minChunks: Infinity
    }),

    new ExtractTextPlugin({
      filename: 'styles.css',
      disable: !isProduction
    }),

    new webpack.optimize.AggressiveMergingPlugin(),

    new HtmlWebpackPlugin({
      title: 'Cyberdyne',
      template: `${STATIC_DIR}/index.html`
    }),

    new CopyWebpackPlugin(
      [
        {from: `${STATIC_DIR}/assets`, to: `${BUILD_DIR}/assets`},
        {from: `${STATIC_DIR}/skins`, to: `${BUILD_DIR}/skins`},
        {from: `${STATIC_DIR}/bootstrap`, to: `${BUILD_DIR}/bootstrap`}
      ]
    ),

    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        screw_ie8: true,
        conditionals: true,
        unused: true,
        comparisons: true,
        sequences: true,
        dead_code: true,
        evaluate: true,
        if_return: true,
        join_vars: true,
      },
      output: {
        comments: false,
        ascii_only: true,
      }
    }),
  ],
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx"],
    mainFields: ['browser', 'main']
  }
};
