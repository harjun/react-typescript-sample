import React from 'react';
import { connect } from 'react-redux';

import { getUserProfile, isLoggedIn } from '../../utils/AuthUtil';

import './style.scss';

interface ILoginState {
  email: string;
  password: string;
  isFormValid: boolean;
  agencyId?: number;
  error: {
    email: IFieldValidation;
    password: IFieldValidation;
  };
}

interface ILoginProps extends ICommonProps {
  loginError: string;
  userProfile: IUserProfile;
  loginSuccess: TLoginSuccess;
  loginRequest: TLoginRequest;
}

class Login extends React.Component<ILoginProps, ILoginState> {
  constructor(props: ILoginProps) {
    super(props);

    this.state = {
      email: '',
      password: '',
      isFormValid: true,
      error: {
        email: {
          errorState: IValidationState.SUCCESS,
          errorMessage: '',
        },
        password: {
          errorState: IValidationState.SUCCESS,
          errorMessage: '',
        },
      },
    };
  }

  componentDidMount() {
    if (isLoggedIn()) {
      this.redirectUser();
    }
  }

  redirectUser = () => {
    const userProfile: IUserProfile = getUserProfile();
    this.props.loginSuccess(userProfile);

    this.props.history.push('/');
  };

  clearValidationStatus() {
    const cleanState = {
      email: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
      password: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
    };

    const newState: ILoginState = this.state;
    newState.error = cleanState;

    this.setState(newState);
  }

  validateForm() {
    this.clearValidationStatus();
    const newState: ILoginState = this.state;
    let isValid = true;

    if (!this.state.email) {
      newState.error.email.errorState = IValidationState.ERROR;
      newState.error.email.errorMessage = 'Email cannot be empty';

      isValid = false;
    }

    if (!this.state.password) {
      newState.error.password.errorState = IValidationState.ERROR;
      newState.error.password.errorMessage = 'Password cannot be empty';

      isValid = false;
    }

    newState.isFormValid = isValid;

    this.setState(newState);
  }

  loginFailed() {
    this.clearValidationStatus();

    const newState: ILoginState = this.state;
    newState.error.email.errorState = IValidationState.ERROR;
    newState.error.password.errorState = IValidationState.ERROR;
    newState.error.email.errorMessage = 'Email and password do not match';
    newState.error.password.errorMessage = 'Email and password do not match';

    this.setState(newState);
  }

  handleChange = (event: any) => {
    const newState = this.state;
    newState[event.target.name] = event.target.value;

    this.setState(newState);
  };

  login = () => {
    this.validateForm();

    if (this.state.isFormValid) {
      this.props
        .loginRequest(this.state.email, this.state.password)
        .then(action => {
          // Listen to the action and do some operation.
        });
    }
  };

  onKeyDown = e => {
    if (e.key === 'Enter') {
      this.login();
    }
  };

  render() {
    return (
      <div className="login-container">
        <div className="login-form" onKeyDown={this.onKeyDown}>
          <div className="login-form__logo">
            <img src="/assets/logo/lp-logo-white.png" />
          </div>
          <div className="login-form__fields" />
          <div className="login-form__actions">
            <span>
              <a href="/forgot-password">Forgot Password?</a>
            </span>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  userProfile: state.auth.userProfile,
  loginError: state.auth.error,
});

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
