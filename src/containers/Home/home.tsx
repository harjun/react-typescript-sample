import React from 'react';
import { connect } from 'react-redux';
import { Switch, withRouter } from 'react-router';

import SideMenu from '../../components/SideMenu/sideMenu';
import { getUserProfile, isLoggedIn } from '../../utils/AuthUtil';

import './style.scss';

interface IHomeProps extends ICommonProps {
  logOutRequest: TLogout;
  loginSuccess: TLoginSuccess;
  userProfile: IUserProfile;
}

interface IHomeState {
  isSideMenuCollapsed: boolean;
  sideMenuOptions: ISideMenuOption[];
}

class Home extends React.Component<IHomeProps, IHomeState> {
  constructor(props: IHomeProps) {
    super(props);

    this.state = {
      isSideMenuCollapsed: false,
      sideMenuOptions: [],
    };
  }

  componentWillMount() {
    if (isLoggedIn()) {
      const UserProfile: IUserProfile = getUserProfile();

      this.props.loginSuccess(UserProfile);
    }

    if (this.props.userProfile) {
      this.setNavLinks(this.props.userProfile);
    }
  }

  componentWillReceiveProps(nextProps: IHomeProps) {
    if (nextProps.userProfile) {
      this.setNavLinks(nextProps.userProfile);
    }
  }

  setNavLinks = (userProfile: IUserProfile) => {
    let sideMenuOptions: ISideMenuOption[] = [];
    if (userProfile.scopes.type === USER_TYPE.ACCELAVAR) {
      sideMenuOptions = [
        {
          id: 1,
          name: 'Dashboard',
          path: '/',
          icon: 'assets/icons/home.png',
        },
        {
          id: 2,
          name: 'Providers',
          path: '/providers',
          icon: 'assets/icons/responsive.png',
        },
        {
          id: 3,
          name: 'AceelaVAR Users',
          path: '/accelavar-users',
          icon: 'assets/icons/multiple-users-silhouette.png',
        },
      ];
    } else if (userProfile.scopes.type === USER_TYPE.PROVIDER) {
      sideMenuOptions = [
        {
          id: 1,
          name: 'Dashboard',
          path: '/',
          icon: 'assets/icons/home.png',
        },
        {
          id: 2,
          name: 'Inventory Management',
          path: '/inventory-management',
          icon: 'assets/icons/responsive.png',
        },
        {
          id: 3,
          name: 'Customer Configuration',
          path: '/customer-configuration',
          icon: 'assets/icons/user.png',
        },
        {
          id: 4,
          name: 'Users and Access',
          path: '/provider-users',
          icon: 'assets/icons/multiple-users-silhouette.png',
        },
        {
          id: 5,
          name: 'Documentation',
          path: '/documentation',
          icon: 'assets/icons/text-document.png',
        },
        {
          id: 6,
          name: 'Report',
          path: '/reports',
          icon: 'assets/icons/report.png',
        },
      ];
    } else if (userProfile.scopes.type === USER_TYPE.CUSTOMER) {
      sideMenuOptions = [
        {
          id: 1,
          name: 'Dashboard',
          path: '/',
          icon: 'assets/icons/home.png',
        },
        {
          id: 2,
          name: 'Inventory Management',
          path: '/inventory-management',
          icon: 'assets/icons/responsive.png',
        },
        {
          id: 3,
          name: 'Documentation',
          path: '/documentation',
          icon: 'assets/icons/text-document.png',
        },
        {
          id: 4,
          name: 'Report',
          path: '/reports',
          icon: 'assets/icons/report.png',
        },
        {
          id: 5,
          name: 'Service Request',
          path: '/service-requests',
          icon: 'assets/icons/report.png',
        },
      ];
    }

    this.setState({ sideMenuOptions });
  };

  logout = () => {
    this.props.logOutRequest();
    this.props.history.push('/login');
  };

  toggleSideMenuCollapse = (Event: any) => {
    this.setState(prevState => ({
      isSideMenuCollapsed: !prevState.isSideMenuCollapsed,
    }));
  };

  render() {
    return (
      <div
        className={`app-grid ${
          this.state.isSideMenuCollapsed ? 'app-grid--side-menu-collapsed' : ''
        }`}
      >
        <SideMenu
          logout={this.logout}
          isCollapsed={this.state.isSideMenuCollapsed}
          toggleCollapseState={this.toggleSideMenuCollapse}
          sideMenuOptions={this.state.sideMenuOptions}
          location={this.props.location}
        />
        <div className="app-holder">
          <h1> Header goes here </h1>
          <div className="app-body">
            <Switch />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  userProfile: state.auth.userProfile,
});

const mapDispatchToProps = (dispatch: any) => ({});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home));
