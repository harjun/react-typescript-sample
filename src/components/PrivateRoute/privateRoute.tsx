import React from 'react';
import { Redirect, Route } from 'react-router-dom';

import { isLoggedIn } from '../../utils/AuthUtil';

const redirectLoginPage = (rest: any) => {
  const { agencyId } = rest.computedMatch.params;
  const redirectUrl = agencyId ? `/agencies/${agencyId}/login` : '/login';

  return (
    <Route
      {...rest}
      render={props => {
        return (
          <Redirect
            to={{
              pathname: redirectUrl,
              state: { from: rest.location },
            }}
          />
        );
      }}
    />
  );
};

// const redirectAdminHomePage = (rest: any) => {
//   const redirectUrl = '/';

//   return (
//     <Route
//       {...rest}
//       render={props => {
//         return (
//           <Redirect
//             to={{
//               pathname: redirectUrl,
//               state: { from: rest.location },
//             }}
//           />
//         );
//       }}
//     />
//   );
// };

// const redirectAgencyHomePage = (rest: any) => {
//   const { agencyId } = rest.computedMatch.params;
//   const redirectUrl = `/agencies/${agencyId}`;

//   return (
//     <Route
//       {...rest}
//       render={props => {
//         return (
//           <Redirect
//             to={{
//               pathname: redirectUrl,
//               state: { from: rest.location },
//             }}
//           />
//         );
//       }}
//     />
//   );
// };

// const isAuthorizedAgency = (rest: any, userProfile:
// IUserProfile): boolean => {
//   if (
//     userProfile.agencyId !== parseInt(rest.computedMatch.params.agencyId, 10)
//   ) {
//     return false;
//   }

//   return true;
// };

// const isAuthorizedProduct = (rest: any, userProfile:
// IUserProfile): boolean => {
//   const { productId } = rest.computedMatch.params;
//   const { productScope } = userProfile.scopes;

//   if (!productScope[productId] || !productScope[productId].isProductAdmin) {
//     return false;
//   }

//   return true;
// };

// const isProductAdmin = (rest: any, userProfile: IUserProfile): boolean => {
//   const { productScope } = userProfile.scopes;
//   for (const productId of Object.keys(productScope)) {
//     if (productScope[productId].isProductAdmin) {
//       return true;
//     }
//   }

//   return false;
// };

// const hasProductPermissions = (
//   rest: any,
//   userProfile: IUserProfile
// ): boolean => {
//   const productId = parseInt(rest.computedMatch.params.productId, 10);
//   const { productScope } = userProfile.scopes;

//   if (!productScope[productId]) {
//     return false;
//   } else {
//     const productACL = productScope[productId].acl;
//     if (productACL === ProductACL.NONE) {
//       return false;
//     }
//   }

//   return true;
// };

const PrivateRoute = ({ component: Component, ...rest }) => {
  // const userProfile = getUserProfile();

  if (!isLoggedIn()) {
    return redirectLoginPage(rest);
  }

  // if (rest.acl) {
  //   const acl: ACL = rest.acl;

  //   if (acl === ACL.COMCATE_ADMIN) {
  //     if (!userProfile.scopes.comcateAdmin) {
  //       return redirectAdminHomePage(rest);
  //     }
  //   }

  //   if (acl === ACL.APP_ADMIN) {
  //     if (
  //       !isAuthorizedAgency(rest, userProfile) ||
  //       (!userProfile.scopes.siteAdmin && !isProductAdmin(rest, userProfile))
  //     ) {
  //       return redirectAgencyHomePage(rest);
  //     }
  //   }

  //   if (acl === ACL.SITE_ADMIN) {
  //     if (
  //       !isAuthorizedAgency(rest, userProfile) ||
  //       !userProfile.scopes.siteAdmin
  //     ) {
  //       return redirectAgencyHomePage(rest);
  //     }
  //   }

  //   if (acl === ACL.PRODUCT_ADMIN) {
  //     if (
  //       !isAuthorizedAgency(rest, userProfile) ||
  //       !isAuthorizedProduct(rest, userProfile)
  //     ) {
  //       return redirectAgencyHomePage(rest);
  //     }
  //   }

  //   if (acl === ACL.STAFF) {
  //     if (
  //       !isAuthorizedAgency(rest, userProfile) ||
  //       !hasProductPermissions(rest, userProfile)
  //     ) {
  //       return redirectAgencyHomePage(rest);
  //     }
  //   }
  // }

  return (
    <Route
      {...rest}
      render={props => {
        return <Component {...props} />;
      }}
    />
  );
};

export default PrivateRoute;
