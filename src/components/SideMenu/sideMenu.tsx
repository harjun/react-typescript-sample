import React from 'react';
import { Link } from 'react-router-dom';

import './style.scss';

interface ISideMenuProps {
  isCollapsed: boolean;
  toggleCollapseState: (e: any) => void;
  logout: TLogout;
  sideMenuOptions: ISideMenuOption[];
  location: any;
}

interface ISideMenuState {
  isActive: number;
}

export default class SideMenu extends React.Component<
  ISideMenuProps,
  ISideMenuState
> {
  constructor(props: ISideMenuProps) {
    super(props);

    this.state = { isActive: null };
  }

  componentDidMount() {
    this.setActiveOnLoad();
  }

  setActiveOnLoad = () => {
    this.props.sideMenuOptions.forEach((option: any) => {
      if (this.props.location.pathname === option.path) {
        this.setState({ isActive: option.id });
      }
    });
  };

  setActiveTab = (id: any) => {
    this.setState({ isActive: id });
  };

  logout = () => {
    this.props.logout();
  };

  render() {
    return (
      <div className="side-menu">
        <div
          className="side-menu__collapse-toggle"
          onClick={this.props.toggleCollapseState}
        >
          <button />
        </div>
        <div className="side-menu__logo">
          <img alt="" src="assets/logo/lp-logo-white.png" />
        </div>
        <div className="side-menu__options">
          {this.props.sideMenuOptions.map(option => (
            <Link
              to={`${option.path}`}
              onClick={() => this.setActiveTab(option.id)}
              key={option.id}
              className={
                this.state.isActive === option.id
                  ? 'side-menu__option side-menu__option--active'
                  : 'side-menu__option'
              }
            >
              <img alt="" src={`${option.icon}`} />
              <span>{`${option.name}`}</span>
            </Link>
          ))}
        </div>
        <div className="side-menu__fixed-options">
          <Link to="/setting" className="side-menu__option">
            <img alt="" src="assets/icons/settings.png" />
            <span>Setting</span>
          </Link>
          <div className="side-menu__option" onClick={() => this.logout()}>
            <img alt="" src="assets/icons/logout.png" />
            <span>Logout</span>
          </div>
        </div>
      </div>
    );
  }
}
