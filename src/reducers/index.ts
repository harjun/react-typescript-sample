import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';

import { LOGOUT_SUCCESS } from '../actions/appState';
import auth from './auth';
import customer from './customer';
import provider from './provider';
import providerAdminUser from './providerAdminUser';
import superuser from './superuser';
import user from './user';
import userType from './userType';

const appReducers = combineReducers<IReduxStore>({
  routing: routerReducer,
  auth,
  user,
  superuser,
  customer,
  provider,
  providerAdminUser,
  userType,
});

export const reducers = (state, action) => {
  let newState = Object.assign({}, state);

  switch (action.type) {
    case LOGOUT_SUCCESS:
      newState = undefined;
      break;
  }

  return appReducers(newState, action);
};
