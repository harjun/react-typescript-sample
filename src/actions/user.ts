import { CALL_API } from '../middleware/ApiMiddleware';

export const USERS_FETCH_REQUEST = 'USERS_FETCH_REQUEST';
export const USERS_FETCH_SUCCESS = 'USERS_FETCH_SUCCESS';
export const USERS_FETCH_FAILURE = 'USERS_FETCH_FAILURE';

export const PRODUCT_USERS_FETCH_REQUEST = 'PRODUCT_USERS_FETCH_REQUEST';
export const PRODUCT_USERS_VIEW_ACL_FETCH_SUCCESS =
  'PRODUCT_USERS_VIEW_ACL_FETCH_SUCCESS';
export const PRODUCT_USERS_CREATE_ACL_FETCH_SUCCESS =
  'PRODUCT_USERS_CREATE_ACL_FETCH_SUCCESS';
export const PRODUCT_USERS_MODIFY_ACL_FETCH_SUCCESS =
  'PRODUCT_USERS_MODIFY_ACL_FETCH_SUCCESS';
export const PRODUCT_USERS_FETCH_FAILURE = 'PRODUCT_USERS_FETCH_FAILURE';

export const USER_FETCH_REQUEST = 'USER_FETCH_REQUEST';
export const USER_FETCH_SUCCESS = 'USER_FETCH_SUCCESS';
export const USER_FETCH_FAILURE = 'USER_FETCH_FAILURE';

export const USER_CREATE_REQUEST = 'USER_CREATE_REQUEST';
export const USER_CREATE_SUCCESS = 'USER_CREATE_SUCCESS';
export const USER_CREATE_FAILURE = 'USER_CREATE_FAILURE';

export const USER_UPDATE_REQUEST = 'USER_UPDATE_REQUEST';
export const USER_UPDATE_SUCCESS = 'USER_UPDATE_SUCCESS';
export const USER_UPDATE_FAILURE = 'USER_UPDATE_FAILURE';

export const USER_DELETE_REQUEST = 'USER_DELETE_REQUEST';
export const USER_DELETE_SUCCESS = 'USER_DELETE_SUCCESS';
export const USER_DELETE_FAILURE = 'USER_DELETE_FAILURE';

export const USER_BULK_MODIFY_REQUEST = 'USER_BULK_MODIFY_REQUEST';
export const USER_BULK_MODIFY_SUCCESS = 'USER_BULK_MODIFY_SUCCESS';
export const USER_BULK_MODIFY_FAILURE = 'USER_BULK_MODIFY_FAILURE';

export const fetchUsers = () => ({
  [CALL_API]: {
    endpoint: 'users',
    method: 'get',
    authenticated: false,
    types: [USERS_FETCH_REQUEST, USERS_FETCH_SUCCESS, USERS_FETCH_FAILURE],
  },
});

export const fetchUsers1 = (agencyId: number) => ({
  [CALL_API]: {
    endpoint: `agencies/${agencyId}/staff`,
    method: 'get',
    authenticated: true,
    types: [USERS_FETCH_REQUEST, USERS_FETCH_SUCCESS, USERS_FETCH_FAILURE],
  },
});

export const fetchUser = (agencyId: number, userId: number) => ({
  [CALL_API]: {
    endpoint: `agencies/${agencyId}/staff/${userId}`,
    method: 'get',
    authenticated: true,
    types: [USER_FETCH_REQUEST, USER_FETCH_SUCCESS, USER_FETCH_FAILURE],
  },
});

export const createUser = (agencyID: number, user: IUser) => ({
  [CALL_API]: {
    endpoint: `agencies/${agencyID}/staff`,
    method: 'post',
    body: user,
    authenticated: true,
    types: [USER_CREATE_REQUEST, USER_CREATE_SUCCESS, USER_CREATE_FAILURE],
  },
});

export const updateUser = (agencyId: number, userId: number, user: IUser) => ({
  [CALL_API]: {
    endpoint: `agencies/${agencyId}/staff/${userId}`,
    method: 'put',
    body: user,
    authenticated: true,
    types: [USER_UPDATE_REQUEST, USER_UPDATE_SUCCESS, USER_UPDATE_FAILURE],
  },
});

export const usersBulkModify = (agencyId: number, users: IBulkModify) => ({
  [CALL_API]: {
    endpoint: `agencies/${agencyId}/staff/bulkModify`,
    method: 'post',
    body: users,
    authenticated: true,
    types: [
      USER_BULK_MODIFY_REQUEST,
      USER_BULK_MODIFY_SUCCESS,
      USER_BULK_MODIFY_FAILURE,
    ],
  },
});
