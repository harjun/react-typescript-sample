import jwtDecode from 'jwt-decode';

const ID_TOKEN = 'id_token';
const PROFILE_EXPIRY = 'profile_expiry';
const PROFILE = 'profile';

export const storeUserProfile = (jwtToken: string) => {
  const userProfile = jwtDecode(jwtToken);

  localStorage.setItem(ID_TOKEN, jwtToken);
  localStorage.setItem(PROFILE, JSON.stringify(userProfile));
  localStorage.setItem(PROFILE_EXPIRY, userProfile.exp);

  return userProfile;
};

export const userLogOut = () => {
  localStorage.removeItem(ID_TOKEN);
  localStorage.removeItem(PROFILE);
  localStorage.removeItem(PROFILE_EXPIRY);
};

export const isLoggedIn = () => {
  const expiry = localStorage.getItem(PROFILE_EXPIRY);
  const userProfile = localStorage.getItem(PROFILE);

  if (expiry !== null && userProfile !== null) {
    const expiryDate = new Date(0);
    expiryDate.setUTCSeconds(Number(expiry));

    if (new Date() < expiryDate) {
      return true;
    }
  }

  return false;
};

export const getUserProfile = (): IUserProfile => {
  return JSON.parse(localStorage.getItem(PROFILE));
};

export const getIdToken = () => {
  return localStorage.getItem(ID_TOKEN);
};
