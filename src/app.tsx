import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';

import PrivateRoute from './components/PrivateRoute/privateRoute';
import Home from './containers/Home/home';
import ForgotPassword from './containers/Login/forgotPassword';
import Login from './containers/Login/login';
import ResetPassword from './containers/Login/resetPassword';
import { setAPIBaseUrl as initAPIMiddleware } from './middleware/ApiMiddleware';
import { setAPIBaseUrl as initAuthMiddleware } from './middleware/AuthMiddleware';
import store, { history } from './store';

import Setting from './containers/Settings/SettingCW';
import './main.scss';

const initApp = () => {
  initAPIMiddleware(window.location.hostname);
  initAuthMiddleware(window.location.hostname);
};

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        <Route exact path="/login" component={Login} />
        <Route path="/setting" component={Setting} />
        <Route exact path="/forgot-password" component={ForgotPassword} />
        <Route
          exact
          path="/set-password/:uid/:token/"
          component={ResetPassword}
        />
        <PrivateRoute path="/" component={Home} />
      </Switch>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('app'),
  initApp
);
