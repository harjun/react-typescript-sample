import createHistory from 'history/createBrowserHistory';
import { routerMiddleware } from 'react-router-redux';
import { applyMiddleware, createStore as _createStore } from 'redux';
import { reducers as combinedReducers } from '../reducers';

import ApiMiddleware from '../middleware/ApiMiddleware';
import {
  LoginMiddleware,
  LogOutMiddleware,
} from '../middleware/AuthMiddleware';

export const history = createHistory();

const reduxRouterMiddleware = routerMiddleware(history);
const middleware = [
  reduxRouterMiddleware,
  LoginMiddleware,
  LogOutMiddleware,
  ApiMiddleware,
];

const store = _createStore(combinedReducers, applyMiddleware(...middleware));

export default store;
