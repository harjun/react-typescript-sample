// This is where all the API interfaces goes.
declare const enum USER_TYPE {
  ACCELAVAR = 'ACCELAVAR',
  PROVIDER = 'PROVIDER',
  CUSTOMER = 'CUSTOMER',
}

declare const enum USER_ROLE {
  OWNER = 'OWNER',
  ADMIN = 'ADMIN',
  STAFF = 'STAFF',
}

declare interface IUserProfile {
  user_id: string;
  email: string;
  scopes: IAuthScope;
}

declare interface IAuthScope {
  type: USER_TYPE;
  role: USER_ROLE;
}

declare interface IUser {
  id?: number;
  divisionId: number;
  username: string;
  password?: string;
  confirmPassword?: string;
  firstName: string;
  middleName?: string;
  lastName: string;
  email: string;
  phone: string;
  title: string;
  activeDate: string;
  inactiveDate?: string;
  signature?: string;
  preferences?: object;
  isActive?: boolean;
  lastLogin?: string;
  createdOn?: string;
}
