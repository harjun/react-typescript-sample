// All the redux related interfaces and types goes here.
interface IReduxStore {
  routing: any;
  user: IUserStore;
}

declare interface IUserStore {
  currentUser: IUser;
  users: IUser[];
  productUsersViewACL: IUser[];
  productUsersCreateACL: IUser[];
  productUsersModifyACL: IUser[];
}
