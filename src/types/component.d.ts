// This is where all the interfaces for the common component goes.
declare interface IHash {
  [key: string]: any;
}

declare interface ICommonProps {
  history: any;
  match: any;
  location: any;
}

declare type TButtonOnClick = ((event: any) => void);

declare interface ISearchItem {
  name: string;
  value: any;
}
declare interface ISearchOptions {
  title?: string;
  items: ISearchItem[];
}

declare const enum RadioState {
  CHECKED = 'CHECKED',
  UNCHECKED = 'UNCHECKED',
}

declare const enum ButtonStyle {
  PRIMARY = 'primary',
  WARNING = 'warning',
  DANGER = 'danger',
  DEFAULT = 'default',
  SUCCESS = 'success',
  INFO = 'info',
}

