import axios from 'axios';
import { storeUserProfile, userLogOut } from '../utils/AuthUtil';

let API_BASE_URL = '';

export const LOGIN_API = Symbol('LOGIN API');
export const LOGOUT_API = Symbol('LOGOUT API');

export const setAPIBaseUrl = (hostname: string) => {
  API_BASE_URL = `http://${hostname}:` + __API_SERVER_PORT__;
};

export const LoginMiddleware = store => next => action => {
  const loginAPI = action[LOGIN_API];

  // So the middleware doesn't get applied to every single action
  if (typeof loginAPI === 'undefined') {
    return next(action);
  }

  const { email, password, types } = loginAPI;

  // TODO
  // Use the ES6 format while implementing retries.
  // const [requestType, successType, errorType] = types;
  const successType = types[1];
  const errorType = types[2];

  return callApi(email, password).then(
    response =>
      next({
        userProfile: response,
        type: successType,
      }),
    error =>
      next({
        error: error.message || 'There was an error.',
        type: errorType,
      })
  );
};

export const LogOutMiddleware = store => next => action => {
  const logOutAPI = action[LOGOUT_API];

  // So the middleware doesn't get applied to every single action
  if (typeof logOutAPI === 'undefined') {
    return next(action);
  }

  userLogOut();

  return next({ type: 'LOGOUT_SUCCESS' });
};

function callApi(email: string, password: string) {
  const LOGIN_URL = API_BASE_URL + '/api/v1/login';
  const data = { email, password };

  return axios
    .post(LOGIN_URL, data)
    .then((response: any) => {
      if (response.status !== 200) {
        throw Error(response.statusText);
      }

      return storeUserProfile(response.data.token);
    })
    .catch(error => {
      throw error;
    });
}
