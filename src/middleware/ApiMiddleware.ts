import axios from 'axios';
import { getIdToken } from '../utils/AuthUtil';

let API_BASE_URL;

// TODO
// This can be removed
export const setAPIBaseUrl = (hostname: string) => {
  API_BASE_URL = `http://${hostname}:` + __API_SERVER_PORT__;
};

export const CALL_API = Symbol('Call API');

export default store => next => action => {
  const callAPI = action[CALL_API];

  // So the middleware doesn't get applied to every single action
  if (typeof callAPI === 'undefined') {
    return next(action);
  }

  const {
    endpoint,
    method,
    params,
    body,
    headers,
    types,
    authenticated,
    hideLoadingWidget,
  } = callAPI;

  // TODO
  // Use the ES6 format while implementing retries.
  // const [requestType, successType, errorType] = types;
  const successType = types[1];
  const errorType = types[2];

  store.dispatch({ type: IS_FETCHING, hideLoadingWidget });

  /* Passing the authenticated boolean back in our data will let us
   * distinguish between normal and secret quotes
   */
  return callApi(endpoint, method, params, body, headers, authenticated).then(
    response => {
      store.dispatch({ type: FETCHING_COMPLETE, hideLoadingWidget });

      return next({
        response,
        authenticated,
        type: successType,
      });
    },
    error => {
      store.dispatch({
        type: ERROR,
        errorMessage: error.response.data.message,
        hideLoadingWidget,
      });

      return next({
        error: error.message || 'There was an error.',
        type: errorType,
      });
    }
  );
};

function callApi(
  endpoint: string,
  method: string,
  params: any,
  body: any,
  headers: any,
  authenticated: boolean
) {
  API_BASE_URL = `http://${window.location.hostname}:` + __API_SERVER_PORT__;

  const token = getIdToken() || null;
  const mandatoryHeaders = {
    'Content-Type': 'application/json',
  };

  const requestHeaders = Object.assign({}, mandatoryHeaders, headers);
  const config = {
    method,
    params,
    headers: requestHeaders,
    data: body,
    url: API_BASE_URL + endpoint,
  };

  if (authenticated) {
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    } else {
      throw new Error('No token saved!');
    }
  }

  return axios
    .request(config)
    .then((response: any) => {
      // TODO
      // Get the expected response status from the action.
      if (
        response.status !== 200 &&
        response.status !== 201 &&
        response.status !== 204
      ) {
        throw Error(response.statusText);
      }

      return response.data;
    })
    .catch(error => {
      throw error;
    });
}
