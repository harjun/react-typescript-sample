const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

const APP_DIR = path.resolve(__dirname, 'src');
const STATIC_DIR = path.resolve(__dirname, 'static', 'app' );
const BUILD_DIR = path.resolve(__dirname, 'dist', 'app');

const extractSass = new ExtractTextPlugin({
  filename: 'style.css',
});

module.exports = {
  entry: [
    'babel-polyfill',
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://0.0.0.0:4000',
    'webpack/hot/only-dev-server',
    `${APP_DIR}/app.tsx`,
  ],
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js',
    publicPath: '/',
  },
  devtool: 'source-map',
  devServer: {
    publicPath: '/',
    contentBase: './dist/app',
    historyApiFallback: true,
    hot: true,
    host: '0.0.0.0',
    port: 4000,
    disableHostCheck: true
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'awesome-typescript-loader',
            options: {
              configFileName: 'tsconfig.json',
            },
          },
        ],
        include: path.join(__dirname, 'src'),
      },
      {
        test: /\.js$/,
        enforce: "pre",
        exclude: /node_modules/,
        loaders: ['source-map-loader']
      },
      {
        test: /(\.scss|\.css)$/,
        use: extractSass.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                url: false
              }
            },
            {
              loader: 'sass-loader',
              options: {
                url: false
              }
            }
          ],
          // use style-loader in development
          fallback: 'style-loader'
        })
      },
      { test: /\.html$/, use: 'html-loader' },
      { test: /\.png$/, use: 'url-loader?limit=10000' },
      { test: /\.jpg$|\.woff2?$|\.ttf$|\.eot$|\.svg$/, use: 'file-loader' },
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    // enable HMR globally

    new webpack.NamedModulesPlugin(),
    // prints more readable module names in the browser console on HMR updates

    new webpack.NoEmitOnErrorsPlugin(),
    // do not emit compiled assets that include errors

    new HtmlWebpackPlugin({
      title: 'Cyberdyne',
      template: `${STATIC_DIR}/index.html`
    }),

    new CopyWebpackPlugin(
      [
        {from: `${STATIC_DIR}/assets`, to: `${BUILD_DIR}/assets`},
      ]


    ),

    new webpack.DefinePlugin({
      __API_SERVER_PORT__: process.env.server === 'localhost' ?
        JSON.stringify('8000') :
        JSON.stringify('http://lookingpoint-api.herokuapp.com'),
    }),

    extractSass,
  ],
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx"]
  },
  watch: true
};
